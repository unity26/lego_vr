﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public AudioSource audioSource;
    public  AudioClip[] LegoSounds;
   
    static AudioHandler audioHandler;
    // Start is called before the first frame update
    private void Awake()
    {
        audioHandler = this;
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

 public  static  AudioHandler Singlton()
    {
        return audioHandler;
    }


    public void PlaySound(int Index)
    {
        audioSource.clip = LegoSounds[Index];
        audioSource.Play();
    }
}
