﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve;
using Valve.VR;
public class ControllerBehavior : MonoBehaviour
{

    
  
    public float SphereRidus = 0.3f;
    GameObject grabedObject;
   
    bool JunctionsDestroied = false;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (ViveInput.Ins().GetStateDown_TriggerClick(ViveInput.ViveHand.RightHand))
        {
            
            Grab();
        }
        if (ViveInput.Ins().GetStateUp_TriggerClick(ViveInput.ViveHand.RightHand))

        {

            ThrowObject();
        }
        if (grabedObject != null &&  ViveInput.Ins().GetState_TouchPadClick(ViveInput.ViveHand.RightHand))
        {
            grabedObject.GetComponent<CollisionDetector>().Bind();
        }
       
    }
    void Grab()
    {

        var colldiers = Physics.OverlapSphere(transform.position, SphereRidus);
        if (colldiers.Length > 0)
        {
            
            
            var colli=System.Array.Find(colldiers, a => a.gameObject.CompareTag("lego"));
            grabedObject = colli.gameObject;
          
            //grabedObject.transform.SetParent(transform);
            
          
           // grabedObject.transform.position = transform.position+(transform.forward*0.08f);
           // grabedObject.transform.right = -transform.forward;
            var collisionDetector = grabedObject.GetComponent<CollisionDetector>();
          collisionDetector.Grabed = true;
            collisionDetector.BreakJoints();
            
            
            
           
            
            grabedObject.GetComponent<Rigidbody>().isKinematic = true;
           
        }

    }

    void ThrowObject()
    {
        Debug.Log("Relased");
        if (grabedObject != null)
        {

           
          //  grabedObject.GetComponent<Rigidbody>().isKinematic = false;
          // 
          //  grabedObject.transform.SetParent(null);
          //  grabedObject = null;

            


        }
    }
    private void OnDrawGizmos()
    {

        Gizmos.DrawWireSphere(transform.position, SphereRidus);
    }

    

}
