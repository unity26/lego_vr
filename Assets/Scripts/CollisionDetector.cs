﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
   public GameObject[] Sockets;
  public List<GameObject> BindedObjects; 
    public float raylength = 1;
    public float JunctionHight = 0.01f;
    public bool Grabed;
  public  bool Conected=false;
    List<Vector2> HitPoints;
    List<Vector2> SourcePoints;
    Vector2 Intpo;
  public  ConfigurableJoint joint;
    public bool snap = false;
  
    // Start is called before the first frame update
    void Start()
    {
        BindedObjects = new List<GameObject>();
        HitPoints = new List<Vector2>();
        SourcePoints = new List<Vector2>();
        Sockets = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            Sockets[i] = transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //if ((ViveInput.Ins().GetStateDown_TriggerClick(ViveInput.ViveHand.RightHand)||Input.GetMouseButtonDown(0)) &&Grabed)
        //{

        //    BreakJoints();
        //    Grabed = true;


        //}
        //if ((ViveInput.Ins().GetStateUp_TriggerClick(ViveInput.ViveHand.RightHand) ||Input.GetMouseButtonUp(0))&& !Grabed)
        //{
        //    Bind();
            
        //}

    }

    void OnDrawGizmos()
    {
       // Gizmos.DrawIcon(new Vector3(Intpo.x, 0.1666534f, Intpo.y),"Centroid");
        if (Sockets != null)
        {
            foreach (var socket in Sockets)
            {
               Vector3 AdPos = new Vector3(socket.transform.position.x, socket.transform.position.y+.02f, socket.transform.position.z);
                Gizmos.DrawRay(AdPos, -socket.transform.up * raylength);
                
            }
        }
    }
    private void OnJointBreak(float breakForce)
    {
        if(BindedObjects.Count>0)
        BindedObjects.Clear();

        print("chianBroke");
        Conected = false;
    }
    public void BreakJoints()
    {
        Conected = false;
       var  joints=GetComponents<ConfigurableJoint>();
        foreach (var joint in joints)
        {
            Destroy(joint);

        }
        
        if (BindedObjects.Count > 0)
        {
            BindedObjects[0].GetComponent<Rigidbody>().freezeRotation = false;
            BindedObjects.Clear();
        }
           
        AudioHandler.Singlton().PlaySound(2);
    }

    void BiLinear()
    {
        List<Vector2> Tp =new List<Vector2>();
    
        while (SourcePoints.Count > 1)
        {
            SourcePoints.Sort((v1, v2) => v1.y.CompareTo(v2.y));
            for (int i = 0; i < SourcePoints.Count-1; i++)
            {
                Intpo= Vector2.Lerp(SourcePoints[i], SourcePoints[i + 1], 0.5f);
                Tp.Add(Intpo);

            }
            SourcePoints.Clear();
            SourcePoints = new List<Vector2>(Tp);
            Tp.Clear();
        }
    }

    void CreateJoint(Vector3 BodyAnchor,Vector3 ConnectionAnchor,Rigidbody rigidbody)
    {
        joint =gameObject.AddComponent<ConfigurableJoint>();
        rigidbody.freezeRotation = true;
        //joint.configuredInWorldSpace = true;
        joint.enablePreprocessing = false;
        joint.autoConfigureConnectedAnchor = false;
        var vv = new Vector3(BodyAnchor.x, transform.position.y, BodyAnchor.z);
        var localanchor = transform.InverseTransformPoint(vv);
        joint.connectedBody = rigidbody;

        Debug.Log(localanchor);
        joint.anchor = localanchor;
        var RigidAnchor= rigidbody.transform.InverseTransformPoint(ConnectionAnchor);
        joint.connectedAnchor = new Vector3(RigidAnchor.x, JunctionHight, RigidAnchor.z);

        joint.xMotion = joint.yMotion = joint.zMotion = ConfigurableJointMotion.Locked;
        joint.angularXMotion = joint.angularYMotion = joint.angularZMotion = ConfigurableJointMotion.Locked;

       
        joint.enableCollision = true;
        joint.enablePreprocessing = true;
        AudioHandler.Singlton().PlaySound(0);
    }
   
   public void Bind()
    {
        

            foreach (var socket in Sockets)
            {
                RaycastHit hit;

                Vector3 AdPos = new Vector3(socket.transform.position.x, socket.transform.position.y + .02f, socket.transform.position.z);
                if (Physics.Raycast(AdPos, -socket.transform.up, out hit, raylength))
                {
                    if (hit.collider.gameObject.CompareTag("soket"))
                    {
                        HitPoints.Add(new Vector2(hit.point.x, hit.point.z));
                        SourcePoints.Add(new Vector2(AdPos.x, AdPos.z));
                        var bbb = hit.collider.gameObject.GetComponentInParent<Rigidbody>();
                        var bb = bbb.gameObject;
                        if (!BindedObjects.Contains(bb))
                        {

                            BindedObjects.Add(bb);

                        }

                    }

                }

            }


        

        if (HitPoints.Count > 0 && !Conected)
        {
            BiLinear();
            Vector3 Conectionpos = new Vector3(Intpo.x,0.14f, Intpo.y);
            if (BindedObjects.Count > 0)
            {
                CreateJoint(Conectionpos, Conectionpos, BindedObjects[0].GetComponent<Rigidbody>());
                Conected = true;
            }
           
            //transform.SetParent(null);
            print(Intpo);
        }

       
    }

public    void SetGrabed(bool Grab)
    {
        Grabed = Grab;
    }

}