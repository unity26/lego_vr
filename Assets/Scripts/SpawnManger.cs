﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManger : MonoBehaviour
{
   public GameObject[] Prefabs;
    public Transform SpawnLocation;
    
    public int CurrentIndex=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      if(ViveInput.Ins().GetStateDown_TouchPadClick(ViveInput.ViveHand.Any)||Input.GetKeyDown(KeyCode.Z))
        {
            Instantiate(Prefabs[CurrentIndex], SpawnLocation.position, SpawnLocation.rotation);
            AudioHandler.Singlton().PlaySound(1);

        }

        if (ViveInput.Ins().GetStateDown_Menu(ViveInput.ViveHand.Any)||Input.GetKeyDown(KeyCode.X))
        {
           CurrentIndex++;
            if (CurrentIndex == 3)
            {
                CurrentIndex = 0;
            }
        }
    }
}
