﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRotateWitthTriggerClick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ViveInput.Ins().GetState_TriggerClick(ViveInput.ViveHand.Any))
        {
            transform.Rotate(0, 10, 0);
        }
    }
}
