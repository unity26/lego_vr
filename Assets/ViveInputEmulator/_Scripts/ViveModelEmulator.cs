﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class ViveModelEmulator : MonoBehaviour
{
    Vector3 worldPos, mousePos;
    [SerializeField]
#if UNITY_EDITOR
    [Help("Rotate: Shift + Mouse \n Move: Shift + W S Q E")]
#endif
    public float RotationSpeed = 2.0f;
    public float MovementSpeed = 2.0f;
    
    private float yaw = 0.0f;
 
    private float pitch = 0.0f;
    private Vector3 posAdd = Vector3.zero;
    private Vector3 currentPos, prevPos;
    public Vector3 velocity;

    public bool UseMouse;
    private float horizontal, vertical;


    // Update is called once per frame
    private void Start()
    {
        prevPos = currentPos = Vector3.zero;
    }
    void Update()
    {
        

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
           
            if (UseMouse)
            {
                yaw += RotationSpeed * Input.GetAxis("Mouse X");
                pitch -= RotationSpeed * Input.GetAxis("Mouse Y");
            }
            else
            {
                horizontal = Input.GetKey(KeyCode.RightArrow) ? 1 : (Input.GetKey(KeyCode.LeftArrow) ? -1 : 0);
                yaw += RotationSpeed * horizontal;

                vertical = Input.GetKey(KeyCode.UpArrow) ? -1 : (Input.GetKey(KeyCode.DownArrow) ? 1 : 0);
                pitch += RotationSpeed * vertical;
            }

            transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

            
            if (Input.GetKey(KeyCode.W))
            {
                transform.position +=(0.001f * MovementSpeed * transform.forward);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position -=(0.001f * MovementSpeed * transform.forward);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += (0.001f * MovementSpeed * transform.right);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= (0.001f * MovementSpeed * transform.right);
            }


            if (Input.GetKey(KeyCode.Q))
            {
                transform.position -= (0.001f* MovementSpeed * transform.up);

            }
            if (Input.GetKey(KeyCode.E))
            {
                transform.position += (0.001f * MovementSpeed * transform.up);
            }

        }

    }

    private void FixedUpdate()
    {
        currentPos = transform.position;

        Vector3 direction = (currentPos*500) - (prevPos*500);
 
        velocity = direction /10;

        prevPos = transform.position;
    }
}
