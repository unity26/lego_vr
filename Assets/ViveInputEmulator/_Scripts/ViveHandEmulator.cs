﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveHandEmulator : MonoBehaviour
{
    public Valve.VR.SteamVR_Behaviour_Pose RealViveController;
    public GameObject FakeViveController;
    [Header("Menu")]
    public KeyCode _menu;


    [Header("TouchPad")]
    public Vector2 TouchPadPos;
    public KeyCode _TouchPadTouch;

    public KeyCode _TouchPadClick;

    [Header("Trigger")]
    public KeyCode _TriggerClick;

    [Range(0, 1)]
    public float TriggerValue;

    [Header("Grip")]
    public KeyCode _GripClick;

    void OnValidate()
    {
        TouchPadPos.x = Mathf.Clamp(TouchPadPos.x, -1, 1);
        TouchPadPos.y = Mathf.Clamp(TouchPadPos.y, -1, 1);
    }



    // Menu
    public bool GetState_Menu()
    {
        return Input.GetKey(_menu);
    }

    public bool GetStateDown_Menu()
    {
        return Input.GetKeyDown(_menu);
    }

    public bool GetStateUp_Menu()
    {
        return Input.GetKeyUp(_menu);
    }

    // TouchPadPos
    public Vector2 GetPos_TouchPad()
    {
        return TouchPadPos;
    }

    // TouchPadTouch
    public bool GetState_TouchPadTouch()
    {
        return Input.GetKey(_TouchPadTouch);
    }

    public bool GetStateDown_TouchPadTouch()
    {
        return Input.GetKeyDown(_TouchPadTouch);
    }

    public bool GetStateUp_TouchPadTouch()
    {
        return Input.GetKeyUp(_TouchPadTouch);
    }

    // TouchPadClick
    public bool GetState_TouchPadClick()
    {
        return Input.GetKey(_TouchPadClick);
    }

    public bool GetStateDown_TouchPadClick()
    {
        return Input.GetKeyDown(_TouchPadClick);
    }

    public bool GetStateUp_TouchPadClick()
    {
        return Input.GetKeyUp(_TouchPadClick);
    }

    // TriggerClick
    public bool GetState_TriggerClick()
    {
        return Input.GetKey(_TriggerClick);
    }

    public bool GetStateDown_TriggerClick()
    {
        return Input.GetKeyDown(_TriggerClick);
    }

    public bool GetStateUp_TriggerClick()
    {
        return Input.GetKeyUp(_TriggerClick);
    }

    // GripClick
    public bool GetState_GripClick()
    {
        return Input.GetKey(_GripClick);
    }

    public bool GetStateDown_GripClick()
    {
        return Input.GetKeyDown(_GripClick);
    }

    public bool GetStateUp_GripClick()
    {
        return Input.GetKeyUp(_GripClick);
    }


    public float GetTriggerValue()
    {
        return TriggerValue;
    }

    public Vector3 GetVelocity(bool useEmulator)
    {
        if (!useEmulator)
            return RealViveController.GetVelocity();

        //return  FakeViveController.transform.forward.normalized ;
        return  FakeViveController.GetComponent<ViveModelEmulator>().velocity;
    }

    public Vector3 GetAngularVelocity(bool useEmulator)
    {
        if (!useEmulator)
            return RealViveController.GetAngularVelocity();

        return FakeViveController.transform.forward.normalized;
    }

}
