﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.XR;
using System.ComponentModel;


public class ViveInput : MonoBehaviour
{
    static ViveInput m_Instance;
    public bool useEmulator;

    [Space(10)]

    ViveLeftHand m_Lefthand;
    ViveRightHand m_RightHand;

    [Header("Menu")]
    public SteamVR_Action_Boolean Menu;
    [Header("TouchPad")]
    public SteamVR_Action_Vector2 TouchPadPos;
    public SteamVR_Action_Boolean TouchPadTouch;
    public SteamVR_Action_Boolean TouchPadClick;
    [Header("Trigger")]
    public SteamVR_Action_Boolean TriggerClick;
    public SteamVR_Action_Single TriggerValue;

    [Header("Grip")]
    public SteamVR_Action_Boolean Grip;

    [Header("Skilton Pos")]
    public SteamVR_Action_Pose pos;




    private void Awake()
    {
        m_Instance = this;
    }
    private void Start()
    {
        m_Lefthand = GetComponent<ViveLeftHand>();
        m_RightHand = GetComponent<ViveRightHand>();
    }
    public static ViveInput Ins()
    {
        return m_Instance;
    }


    private void OnooValidate()
    {
        m_Lefthand = GetComponent<ViveLeftHand>();
        m_RightHand = GetComponent<ViveRightHand>();

        StartCoroutine(ValidateTheRealViveControllerReference());
    }

    IEnumerator ValidateTheRealViveControllerReference()
    {
        yield return new WaitForEndOfFrame();
//        yield return new WaitForSeconds(0.01f);
       
        if (m_Lefthand.RealViveController == null)
        {
            Debug.LogError("<color=red>Error: </color>the Real LeftHand controller is not set in <color=blue>Vive Left Hand</color> !");
            yield return null ;
        }
        else
        {
            if (m_Lefthand.FakeViveController == null)
            {
                Debug.LogError("<color=red>Error: </color>the Fake LeftHand controller is not set in <color=blue>Vive Left Hand</color> !");
            }
            else
            {
                GameObject fake = m_Lefthand.FakeViveController.gameObject;
                GameObject real = m_Lefthand.RealViveController.gameObject;

                for (int i = 1; i < real.transform.childCount; i++)
                    DestroyImmediate(real.transform.GetChild(i).gameObject);

                for (int i = 1; i < fake.transform.childCount; i++)
                {
                    GameObject fakeChild = fake.transform.GetChild(i).gameObject;
                    fakeChild.transform.localPosition = Vector3.zero;
                    fakeChild.transform.rotation = Quaternion.identity;
                    fakeChild.transform.localScale = Vector3.one;

                    GameObject g = Instantiate(fakeChild);
                    g.transform.SetParent(real.transform);
                    g.transform.localPosition = Vector3.zero;
                    g.transform.rotation = Quaternion.identity;
                    g.transform.localScale = Vector3.one;
                }

            }
        }

        if (m_RightHand.RealViveController == null)
        {
            Debug.LogError("<color=red>Error: </color>the Real RightHand controller is not set in <color=blue>Vive Right Hand</color> !");
            yield return null;

        }
        else
        {
            if (m_RightHand.FakeViveController == null)
            {
                Debug.LogError("<color=red>Error: </color>the Fake RightHand controller is not set in <color=blue>Vive Left Hand</color> !");
            }
            else
            {
                GameObject fake = m_RightHand.FakeViveController.gameObject;
                GameObject real = m_RightHand.RealViveController.gameObject;

                for (int i = 1; i < real.transform.childCount; i++)
                    DestroyImmediate(real.transform.GetChild(i).gameObject);

                for (int i = 1; i < fake.transform.childCount; i++)
                {
                    GameObject fakeChild = fake.transform.GetChild(i).gameObject;
                    fakeChild.transform.localPosition = Vector3.zero;
                    fakeChild.transform.rotation = Quaternion.identity;
                    fakeChild.transform.localScale = Vector3.one;

                    GameObject g = Instantiate(fakeChild);
                    g.transform.SetParent(real.transform);
                    g.transform.localPosition = Vector3.zero;
                    g.transform.rotation = Quaternion.identity;
                    g.transform.localScale = Vector3.one;
                }

            }
        }

        if (useEmulator)
        {
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(true);
            m_RightHand.RealViveController.gameObject.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(false);

            m_RightHand.RealViveController.gameObject.transform.parent.gameObject.SetActive(true);
        }


    }

    /// <summary>Returns true while the Menu button is clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetState_Menu(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorState_Menu(hand);

        return Menu.GetState((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorState_Menu(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetState_Menu() || m_RightHand.GetState_Menu();
            case ViveHand.LeftHand:
                return m_Lefthand.GetState_Menu();
            case ViveHand.RightHand:
                return m_RightHand.GetState_Menu();
        }
        return false;

    }

    /// <summary>Returns true once the Menu button is clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateDown_Menu(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateDown_Menu(hand);

        return Menu.GetStateDown((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateDown_Menu(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateDown_Menu() || m_RightHand.GetStateDown_Menu();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateDown_Menu();
            case ViveHand.RightHand:
                return m_RightHand.GetStateDown_Menu();
        }
        return false;

    }

    /// <summary>Returns true once the Menu button is released</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateUp_Menu(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateUp_Menu(hand);

        return Menu.GetStateUp((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateUp_Menu(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateUp_Menu() || m_RightHand.GetStateUp_Menu();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateUp_Menu();
            case ViveHand.RightHand:
                return m_RightHand.GetStateUp_Menu();
        }
        return false;

    }


    ////////////////////////////////////// (TouchPad) Pos
    

    /// <summary>Returns the position on thte TouchPad</summary>
    /// <param name="hand">the hand you want to check</param>
    public Vector2 GetPos_TouchPad(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorPos_TouchPad(hand);

        return TouchPadPos.GetAxis((SteamVR_Input_Sources)hand);
    }
    Vector2 GetEmulatorPos_TouchPad(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return ((m_Lefthand.TouchPadPos.x + m_Lefthand.TouchPadPos.y) > (m_RightHand.TouchPadPos.x + m_RightHand.TouchPadPos.y)) ?
                    m_Lefthand.TouchPadPos : m_RightHand.TouchPadPos;
            case ViveHand.LeftHand:
                return m_Lefthand.TouchPadPos;
            case ViveHand.RightHand:
                return m_RightHand.TouchPadPos;
        }
        return Vector2.zero;
    }

 
    /// <summary>Returns true while the TouchPad is Touched</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetState_TouchPadTouch(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorState_TouchPadTouch(hand);
        return TouchPadTouch.GetState((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorState_TouchPadTouch(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetState_TouchPadTouch() || m_RightHand.GetState_TouchPadTouch();
            case ViveHand.LeftHand:
                return m_Lefthand.GetState_TouchPadTouch();
            case ViveHand.RightHand:
                return m_RightHand.GetState_TouchPadTouch();
        }
        return false;
    }

    /// <summary>Returns true once the TouchPad is Touched</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateDown_TouchPadTouch(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateDown_TouchPadTouch(hand);
        return TouchPadTouch.GetStateDown((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateDown_TouchPadTouch(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateDown_TouchPadTouch() || m_RightHand.GetStateDown_TouchPadTouch();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateDown_TouchPadTouch();
            case ViveHand.RightHand:
                return m_RightHand.GetStateDown_TouchPadTouch();
        }
        return false;
    }

    /// <summary>Returns true once the TouchPad Touch is released</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateUp_TouchPadTouch(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateUp_TouchPadTouch(hand);
        return TouchPadTouch.GetStateUp((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateUp_TouchPadTouch(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateUp_TouchPadTouch() || m_RightHand.GetStateUp_TouchPadTouch();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateUp_TouchPadTouch();
            case ViveHand.RightHand:
                return m_RightHand.GetStateUp_TouchPadTouch();
        }
        return false;
    }

    /// <summary>Returns true while the TouchPad is Clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetState_TouchPadClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorState_TouchPadClick(hand);

        return TouchPadClick.GetState((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorState_TouchPadClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetState_TouchPadClick() || m_RightHand.GetState_TouchPadClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetState_TouchPadClick();
            case ViveHand.RightHand:
                return m_RightHand.GetState_TouchPadClick();
        }
        return false;
    }


    /// <summary>Returns true once the TouchPad is clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateDown_TouchPadClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateDown_TouchPadClick(hand);

        return TouchPadClick.GetStateDown((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateDown_TouchPadClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateDown_TouchPadClick() || m_RightHand.GetStateDown_TouchPadClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateDown_TouchPadClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateDown_TouchPadClick();
        }
        return false;
    }

    /// <summary>Returns true once the TouchPad click is released</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateUp_TouchPadClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateUp_TouchPadClick(hand);

        return TouchPadClick.GetStateUp((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateUp_TouchPadClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateUp_TouchPadClick() || m_RightHand.GetStateUp_TouchPadClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateUp_TouchPadClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateUp_TouchPadClick();
        }
        return false;
    }



    /// <summary>Returns float the trigger analog value ranged from 0:1</summary>
    /// <param name="hand">the hand you want to check</param>
    public float GetValue_Trigger(ViveHand hand)
    {

        if (useEmulator)
        {
            return GetEmulatorValue_Trigger(hand);
        }

        return TriggerValue.GetAxis((SteamVR_Input_Sources)hand);
    }
    float GetEmulatorValue_Trigger(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetTriggerValue() > m_RightHand.GetTriggerValue() ? m_Lefthand.GetTriggerValue() : m_RightHand.GetTriggerValue();
            case ViveHand.LeftHand:
                return m_Lefthand.TriggerValue;
            case ViveHand.RightHand:
                return m_RightHand.GetTriggerValue();
        }
        return 0.0f;
    }

    /// <summary>Returns true while the trigger is Clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetState_TriggerClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorState_TriggerClick(hand);
        return TriggerClick.GetState((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorState_TriggerClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetState_TriggerClick() || m_RightHand.GetState_TriggerClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetState_TriggerClick();
            case ViveHand.RightHand:
                return m_RightHand.GetState_TriggerClick();
        }
        return false;
    }

    /// <summary>Returns true once the trigger is clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateDown_TriggerClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateDown_TriggerClick(hand);
        return TriggerClick.GetStateDown((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateDown_TriggerClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateDown_TriggerClick() || m_RightHand.GetStateDown_TriggerClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateDown_TriggerClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateDown_TriggerClick();
        }
        return false;
    }

    /// <summary>Returns true once the trigger click is released</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateUp_TriggerClick(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateUp_TriggerClick(hand);
        return TriggerClick.GetStateUp((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateUp_TriggerClick(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateUp_TriggerClick() || m_RightHand.GetStateUp_TriggerClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateUp_TriggerClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateUp_TriggerClick();
        }
        return false;
    }

    /// <summary>Returns true while the grip is Clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetState_Grip(ViveHand hand)
    {
 
        if (useEmulator)
        {

            return GetEmulatorState_Grip(hand);
        }
        Debug.Log("SteamVR_Input_Sources");

        return Grip.GetState((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorState_Grip(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetState_GripClick() || m_RightHand.GetState_GripClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetState_GripClick();
            case ViveHand.RightHand:
                return m_RightHand.GetState_GripClick();
        }
        return false;
    }

    /// <summary>Returns true once the grip is clicked</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateDown_Grip(ViveHand hand)
    {
        if (useEmulator)
            return GetEmulatorStateDown_Grip(hand);
        return Grip.GetStateDown((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateDown_Grip(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateDown_GripClick() || m_RightHand.GetStateDown_GripClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateDown_GripClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateDown_GripClick();
        }
        return false;
    }

    /// <summary>Returns true once the grip click is released</summary>
    /// <param name="hand">the hand you want to check</param>
    public bool GetStateUp_Grip(ViveHand hand)
    {
        if (useEmulator)
           return GetEmulatorStateUp_Grip(hand);
        return Grip.GetStateUp((SteamVR_Input_Sources)hand);
    }
    bool GetEmulatorStateUp_Grip(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetStateUp_GripClick() || m_RightHand.GetStateUp_GripClick();
            case ViveHand.LeftHand:
                return m_Lefthand.GetStateUp_GripClick();
            case ViveHand.RightHand:
                return m_RightHand.GetStateUp_GripClick();
        }
        return false;
    }

    /// <summary>if(useEmulator) returns the fake controller forward normalized 
    /// , if(!useEmulator) Returns the Velocity of the RealController</summary>
    /// <param name="hand">the hand you want to check</param>
    public Vector3 GetVelocity(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:                
                return m_Lefthand.GetVelocity(useEmulator).magnitude > m_RightHand.GetVelocity(useEmulator).magnitude ?
                            m_Lefthand.GetVelocity(useEmulator) : m_Lefthand.GetVelocity(useEmulator);
            case ViveHand.LeftHand:
                return m_Lefthand.GetVelocity(useEmulator);
            case ViveHand.RightHand:
                return m_RightHand.GetVelocity(useEmulator);
        }
        return Vector3.zero;
    }

    public Vector3 GetAngularVelocity(ViveHand hand)
    {
        switch (hand)
        {
            case ViveHand.Any:
                return m_Lefthand.GetAngularVelocity(useEmulator).magnitude > m_RightHand.GetAngularVelocity(useEmulator).magnitude ?
                            m_Lefthand.GetAngularVelocity(useEmulator) : m_Lefthand.GetAngularVelocity(useEmulator);
            case ViveHand.LeftHand:
                return m_Lefthand.GetAngularVelocity(useEmulator);
            case ViveHand.RightHand:
                return m_RightHand.GetAngularVelocity(useEmulator);
        }
        return Vector3.zero;
    }


    public enum ViveHand
    {         
        Any =0,        
        LeftHand =1,       
        RightHand = 2,
    }
}
