﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveInputTest : MonoBehaviour
{

    public bool _TestMenu, _TestTouchPad , _TestTrigger , _TestGrip,_TestVelocity ;
  

    // Update is called once per frame
    void Update()
    {
        
        if (_TestMenu)
            TestMenu();
        if (_TestTouchPad)
            TestTouchPad();
        if (_TestTrigger)
            TestTrigger();
        if (_TestGrip)
            TestGrip();
        if (_TestVelocity)
            TestVelocity();

    }

    public void TestMenu()
    {

        if (ViveInput.Ins().GetStateDown_Menu(ViveInput.ViveHand.LeftHand))
        {
            Debug.Log("StateDown Menu LeftHand");
        }

        if (ViveInput.Ins().GetState_Menu(ViveInput.ViveHand.LeftHand))
        {
            Debug.Log("State Menu LeftHand");
        }

        if (ViveInput.Ins().GetStateUp_Menu(ViveInput.ViveHand.LeftHand))
        {
            Debug.Log("StateUp Menu LeftHand");
        }

    }

    public void TestTouchPad()
    {

        Debug.Log("TouchPadPos Left = "+ViveInput.Ins().GetPos_TouchPad(ViveInput.ViveHand.LeftHand));
        Debug.Log("TouchPadPos Right = "+ViveInput.Ins().GetPos_TouchPad(ViveInput.ViveHand.RightHand));
        Debug.Log("TouchPadPos Any = "+ViveInput.Ins().GetPos_TouchPad(ViveInput.ViveHand.Any));

        //TouchPad Touch
        if (ViveInput.Ins().GetStateDown_TouchPadTouch(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateDown TouchPadTouch RightHand");
        }

        if (ViveInput.Ins().GetState_TouchPadTouch(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("State TouchPadTouch RightHand");
        }

        if (ViveInput.Ins().GetStateUp_TouchPadTouch(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateUp TouchPadTouch RightHand");
        }

        //TouchPad Click
        if (ViveInput.Ins().GetStateDown_TouchPadClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateDown TouchPadclick RightHand");
        }

        if (ViveInput.Ins().GetState_TouchPadClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("State TouchPadclick RightHand");
        }

        if (ViveInput.Ins().GetStateUp_TouchPadClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateUp TouchPadclick RightHand");
        }
 
    }

    public void TestTrigger()
    {
        // GetTriggerValue
        Debug.Log("Right Trigger = " + ViveInput.Ins().GetValue_Trigger(ViveInput.ViveHand.RightHand));
        Debug.Log("Left Trigger = " + ViveInput.Ins().GetValue_Trigger(ViveInput.ViveHand.LeftHand));
        Debug.Log("Any Trigger = " + ViveInput.Ins().GetValue_Trigger(ViveInput.ViveHand.Any));

        // Trigger Click
        if (ViveInput.Ins().GetStateDown_TriggerClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateDown Trigger RightHand");
        }

        if (ViveInput.Ins().GetState_TriggerClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("State Trigger RightHand");
        }

        if (ViveInput.Ins().GetStateUp_TriggerClick(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateUp Trigger RightHand");
        } 
    }

    public void TestGrip()
    {
       
       
        if (ViveInput.Ins().GetStateDown_Grip(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateDown Grip RightHand");
        }

        if (ViveInput.Ins().GetState_Grip(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("State Grip RightHand");
        }
       
        if (ViveInput.Ins().GetStateUp_Grip(ViveInput.ViveHand.RightHand))
        {
            Debug.Log("StateUp Grip RightHand");
        } 
    }

    public void TestVelocity()
    {
        Debug.Log("AngolarVelocity = " + ViveInput.Ins().GetAngularVelocity(ViveInput.ViveHand.LeftHand));
        Debug.Log("Velocity = " + ViveInput.Ins().GetVelocity(ViveInput.ViveHand.LeftHand));

    }
}
